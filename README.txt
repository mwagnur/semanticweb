Task5 (requires Maven and Java 8):
-Start Fuseki following Task4 instructions below (must publish to http://localhost:3030/Ass2Group17)
-cd to Task5 project root
-mvn spring-boot:run
-You now have a Semantic Search application running on http://localhost:8080/

Task4:
-Install Fuseki
-create empty folder for Triple Store ( --> (Path to Triple Store))
-Start server with: 
fuseki-server --loc=(Path to Triple Store) /Ass2Group17

-http://localhost:3030/
-add data -> select the rdf from Task3 -> upload
-write ALL them SPARQLS you ever wanted
-???
-Profit


SPARQL Query:

PREFIX foaf: <http://xmlns.com/foaf/0.1/>
PREFIX tu:  <http://ifs.tuwien.ac.at/tulid/group17#>

SELECT ?course ?title ?person ?teacherName ?institute ?description
WHERE {
  ?course tu:taughtBy ?person .
  ?person tu:belongsToOU ?institute.
  ?person foaf:name ?teacherName.
  ?course tu:courseTitle ?title .
  ?course tu:courseDescription ?description .
}
LIMIT 100