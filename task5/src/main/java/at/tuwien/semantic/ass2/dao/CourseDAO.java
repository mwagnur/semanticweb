package at.tuwien.semantic.ass2.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;

import at.tuwien.semantic.ass2.model.Course;

public class CourseDAO {

	private static String sparqlService = "http://localhost:3030/Ass2Group17/sparql";

	public List<Course> getCoursesForPerson(String personId) {

		String queryString = "PREFIX foaf: <http://xmlns.com/foaf/0.1/>\n"
				+ "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n"
				+ "PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>\n"
				+ "prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n"
				+ "prefix owl: <http://www.w3.org/2002/07/owl#>\n"
				+ "PREFIX tu:  <http://ifs.tuwien.ac.at/tulid/group17#>\n" +

		"SELECT ?person ?institute ?title ?description \n" +

		"WHERE {\n" + "  ?course tu:taughtBy ?person .\n" + "  ?person tu:belongsToOU ?institute.\n"
				+ "  ?person foaf:name ?teacherName.\n" + "  ?course tu:courseTitle ?title .\n"
				+ "  ?course tu:courseDescription ?description .\n" + "}\n";

		Query query = QueryFactory.create(queryString);
		QueryExecution qe = QueryExecutionFactory.sparqlService(sparqlService, query);

		List<Course> courses = new ArrayList<Course>();

		try {
			ResultSet results = qe.execSelect();

			while (results.hasNext()) {
				QuerySolution solution = results.nextSolution();
				String id = solution.get("?person").toString();

				if (id.equals(personId)) {
					Course course = new Course();
					// String institute = solution.get("?institute").toString();
					String title = solution.get("?title").toString();
					String description = solution.get("?description").toString();

					course.setName(title);
					course.setDescription(description);

					courses.add(course);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			qe.close();
		}

		return courses;
	}

}
