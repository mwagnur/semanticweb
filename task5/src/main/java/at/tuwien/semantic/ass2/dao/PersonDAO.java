package at.tuwien.semantic.ass2.dao;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;

import at.tuwien.semantic.ass2.model.Course;
import at.tuwien.semantic.ass2.model.Person;

public class PersonDAO {

	private static String sparqlService = "http://localhost:3030/Ass2Group17/sparql";
	
	private CourseDAO courseDao = new CourseDAO();

	public List<Person> getAll() {

		String queryString = "PREFIX foaf: <http://xmlns.com/foaf/0.1/>\n"
				+ "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n"
				+ "PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>\n"
				+ "prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n"
				+ "prefix owl: <http://www.w3.org/2002/07/owl#>\n"
				+ "PREFIX tu:  <http://ifs.tuwien.ac.at/tulid/group17#>\n" +

		"SELECT ?teacherName ?person\n" +

		"WHERE {\n" + "  ?course tu:taughtBy ?person .\n" + "  ?person tu:belongsToOU ?institute.\n"
				+ "  ?person foaf:name ?teacherName.\n" + "  ?course tu:courseTitle ?title .\n"
				+ "  ?course tu:courseDescription ?description .\n" + "}\n";

		Query query = QueryFactory.create(queryString);
		QueryExecution qe = QueryExecutionFactory.sparqlService(sparqlService, query);

		List<Person> people = new ArrayList<Person>();

		try {
			ResultSet results = qe.execSelect();

			Set<String> uniqueIds = new HashSet<String>();

			while (results.hasNext()) {
				QuerySolution solution = results.nextSolution();
				String id = solution.get("?person").toString();
				String name = solution.get("?teacherName").toString();

				if (!uniqueIds.contains(id)) {
					uniqueIds.add(id);
					Person person = new Person();
					person.setId(id);
					person.setName(name);
					List<Course> courses = courseDao.getCoursesForPerson(id);
					person.setCourses(courses);
					people.add(person);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			qe.close();
		}

		return people;
	}

}
