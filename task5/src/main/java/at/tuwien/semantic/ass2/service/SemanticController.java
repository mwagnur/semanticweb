package at.tuwien.semantic.ass2.service;

import java.util.List;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import at.tuwien.semantic.ass2.dao.PersonDAO;
import at.tuwien.semantic.ass2.model.Person;

@RestController
public class SemanticController {
	
	private PersonDAO personDao = new PersonDAO();

	@RequestMapping("/getPeople")
	public List<Person> getPeople() {
		List<Person> people = personDao.getAll();
		return people;
	}

}
