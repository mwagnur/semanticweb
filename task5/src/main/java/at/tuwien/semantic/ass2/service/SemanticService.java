package at.tuwien.semantic.ass2.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SemanticService {

	public static void main(String[] args) {
		SpringApplication.run(SemanticService.class, args);
	}

}