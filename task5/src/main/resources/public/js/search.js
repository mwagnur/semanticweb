var staffData = {};

var cortical;

var currentResults = [];
var pendingComparisons = 0;

var dataLoaded = false;

$(document).ready(function () {
    cortical = new Cortical.CoreClient("7e4ea6d0-5517-11e4-bea4-77acda10ff33");
    loadStaffData();
    $("#search").click(search);
    $(document).keypress(function (e) {
        if (e.which == 13 && dataLoaded) {
            search();
        }
    });
});

function loadStaffData() {
    $.get("http://localhost:8080/getPeople/", function (data) {
        $.each(data, function (index, value) {
            var courses = value.courses;
            var text = "";

            for (var i = 0; i < courses.length; i++) {
                var description = courses[i].description;
                description = cleanString(description);
                text = text + " " + description;
            }

            if (text.length > 10) {
                staffData[value.name] = text;
            }
        });
        updateStatus("Staff data loaded. Ready to search.");
        $("#search").removeClass("disabled");
        dataLoaded = true;
    });
}

function search() {
    var query = $("#query").val();

    if (dataLoaded && query != "") {

        $.each($("#results").children(), function (index, value) {
            setTimeout(function () {
                $(value).animate({opacity: 0}, 200);
            }, index * 20);
        });

        // Ensure query returns a valid fingerprint

        cortical.getFingerprintForText(query, function () {

            updateStatus("Performing search...");
            $("#search").addClass("disabled");

            currentResults = [];

            $.each(staffData, function (name, text) {

                var callback = function (data, name) {
                    currentResults.push([data.cosineSimilarity, name]);
                    pendingComparisons--;

                    if (pendingComparisons == 0) {
                        currentResults = currentResults.sort(function (a, b) {
                            return b[0] - a[0]
                        });
                        $("#search").removeClass("disabled");
                        updateStatus("Search completed.");

                        $("#results").empty();

                        $.each(currentResults.slice(0, 10), function (index, value) {
                            var score = value[0];
                            var name = value[1];
                            score = score * 100;
                            score = Math.floor(score);
                            setTimeout(function () {
                                var $element = $("<li style=\"opacity: 0\"class=\"list-group-item\"><span" + " class=\"badge\">" + score + "%</span>" + name + "</li>");
                                $("#results").append($element);
                                $element.animate({opacity: 1}, 200);
                            }, index * 20);
                        });
                    }
                };

                // Wrap the callback with another function in order to pass "name" to it
                cortical.compare([{text: query}, {text: text}], function (data) {
                    callback(data, name);
                });

                pendingComparisons++;
            });

        });

    }

}

function cleanString(str) {
    var toRemove = ["href", "target", "\n", "↵", " + ", ":", "course", "h1", "h3", "  "];
    for (var i = 0; i < toRemove.length; i++) {
        str = replaceAll(str, toRemove[i], "");
    }
    str = str.replace(/\./g, ' ');
    str = str.trim();
    return str.toLowerCase();
}

function replaceAll(str, find, replace) {
    return str.replace(new RegExp(find, 'g'), replace);
}

function updateStatus(text) {
    var $status = $("#status");
    $status.fadeOut("fast", function () {
        $status.text(text);
        $status.fadeIn("fast");
    });

}